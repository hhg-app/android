package de.hochschulhausgarching.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import de.hochschulhausgarching.R
import de.hochschulhausgarching.backend.BackendCall
import de.hochschulhausgarching.backend.BackendCallback
import de.hochschulhausgarching.backend.requests.LoginRequest
import de.hochschulhausgarching.backend.response.LoginResponse
import kotlinx.android.synthetic.main.login_screen.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_screen)

        // Make Login button only clickable when username and password have been entered
        checkFieldsFilled(etUsername)
        checkFieldsFilled(etPassword)

        btnLogin.setOnClickListener {
            loadingIndicator.visibility = View.VISIBLE
            BackendCall().loginRequest(
                LoginRequest(
                    etUsername.text.toString(),
                    etPassword.text.toString()
                ), object: BackendCallback<LoginResponse> {
                    override fun onSuccess(response: LoginResponse) {
                        loadingIndicator.visibility = View.GONE
                        if (response.error != null) {
                            Toast.makeText(this@LoginActivity, response.error, Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(this@LoginActivity, "Welcome ${response.user.displayName}!", Toast.LENGTH_LONG).show()
                            startActivity(Intent(this@LoginActivity, DashboardActivity::class.java).putExtra("login_response", response.toString()))
                            finish()
                        }
                    }

                    override fun onFailure() {
                        loadingIndicator.visibility = View.GONE
                        Toast.makeText(this@LoginActivity, "Login failed!", Toast.LENGTH_SHORT).show()
                    }

                }
            )

        }

    }

    private fun checkFieldsFilled(editText: EditText) {

        editText.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etUsername.text.isNotEmpty() && etPassword.text.isNotEmpty()) {
                    btnLogin.isEnabled = true
                    btnLogin.alpha = 1f
                } else {
                    btnLogin.isEnabled = false
                    btnLogin.alpha = 0.7f
                }
            }

        })

    }
}