package de.hochschulhausgarching.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import de.hochschulhausgarching.R
import kotlinx.android.synthetic.main.activity_main.*

class DashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        text.text = intent.getStringExtra("login_response")


    }
}