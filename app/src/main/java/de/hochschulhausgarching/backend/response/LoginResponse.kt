package de.hochschulhausgarching.backend.response

class LoginResponse(
    var authtoken: String,
    var user: User,
    var userGroups: UserGroups,
    var error: String?
) {
    override fun toString(): String {
        return "LoginResponse(authtoken='$authtoken', user=$user, userGroups=$userGroups, error='$error')"
    }
}

class User(
    var uid: String,
    var givenName: String,
    var familyName: String,
    var displayName: String,
    var roomNumber: String,
    var balance: String
) {
    override fun toString(): String {
        return "User(uid='$uid', givenName='$givenName', familyName='$familyName', displayName='$displayName', roomNumber='$roomNumber', balance='$balance')"
    }
}

class UserGroups(
    var isAdmin: Boolean,
    var isBarteam: Boolean,
    var isGKTeam: Boolean,
    var isHaussprecher: Boolean,
    var isTutor: Boolean
) {
    override fun toString(): String {
        return "UserGroups(isAdmin=$isAdmin, isBarteam=$isBarteam, isGKTeam=$isGKTeam, isHaussprecher=$isHaussprecher, isTutor=$isTutor)"
    }
}