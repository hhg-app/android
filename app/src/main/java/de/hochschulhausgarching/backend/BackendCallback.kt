package de.hochschulhausgarching.backend

interface BackendCallback<T> {

    fun onSuccess(response: T)

    fun onFailure()

}