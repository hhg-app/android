package de.hochschulhausgarching.backend

import de.hochschulhausgarching.backend.requests.LoginRequest
import de.hochschulhausgarching.backend.response.LoginResponse
import retrofit2.Call
import retrofit2.http.*

private const val CONTENT_TYPE_JSON = "content-type: application/json; charset=utf-8"
private const val HEADER_CLOSE_CONNECTION = "Connection: close"

interface RestAPIEndpoint {

    @POST
    @Headers(CONTENT_TYPE_JSON, HEADER_CLOSE_CONNECTION)
    fun login(@Url url: String,  @Body body : LoginRequest) : Call<LoginResponse>

}