package de.hochschulhausgarching.backend.requests

import com.google.gson.annotations.SerializedName

class LoginRequest(
    @SerializedName("username")
    var username: String,
    var password: String
)