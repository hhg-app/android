package de.hochschulhausgarching.backend

import android.util.Log
import com.google.gson.GsonBuilder
import de.hochschulhausgarching.BACKEND_URL
import de.hochschulhausgarching.backend.requests.LoginRequest
import de.hochschulhausgarching.backend.response.LoginResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit




var CLIENT: BackendCall.BackendConnection? = null

val GSON = GsonBuilder().create()

open class BackendCall() {

    class BackendConnection(val service: RestAPIEndpoint)

    protected fun getService(): RestAPIEndpoint {

        if (CLIENT != null) return CLIENT!!.service

        CLIENT = getClient()
        return CLIENT!!.service

    }

    private fun getClient(): BackendConnection {

        if (CLIENT != null) return CLIENT!!

        val builder = OkHttpClient.Builder()
        builder.connectTimeout(15, TimeUnit.SECONDS)
        builder.readTimeout(15, TimeUnit.SECONDS)
        builder.writeTimeout(15, TimeUnit.SECONDS)

        // Since every request to the onOffice Backend requires a current timestamp,
        // requests that have to be sent multiple times receive error 136 as response (your timestamp is too old or your server time wrong.)
        builder.retryOnConnectionFailure(true)

        builder.addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })


        val restAdapter = Retrofit.Builder()
            .baseUrl(BACKEND_URL)
            .addConverterFactory(GsonConverterFactory.create(GSON))
            .client(builder.build())
            .build()

        CLIENT = BackendConnection(restAdapter.create(RestAPIEndpoint::class.java))
        return CLIENT!!

    }

    fun loginRequest(loginRequest: LoginRequest, callback: BackendCallback<LoginResponse>) {
        val call = getService()::login.invoke("http://10.151.13.5:8000/authenticate", loginRequest)
        call.enqueue(object : retrofit2.Callback<LoginResponse> {

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {


                if (response.body() != null) {
                    response.body()?.let { callback.onSuccess(it) }
                } else {
                    callback.onFailure()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                callback.onFailure()
            }
        })

    }

}
